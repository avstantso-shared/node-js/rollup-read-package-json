const { createFilter } = require('rollup-pluginutils');

function rollupReadPackageJson(opts = {}) {
  const { packageJson } = opts;

  if (!packageJson) {
    throw Error(
      `"packageJson" option as "require('./package.json')" should be specified`
    );
  }

  const filter = createFilter(
    opts.include || ['**/*.ts', '**/*.tsx'],
    opts.exclude
  );

  const { template = 'PACKAGE_JSON' } = opts;

  //const PACKAGE_JSON = require(`../package.json`);
  const requireRegExp = new RegExp(
    `const\\s+${template}\\s*=\\s*require\\(['"${'`'}].+package\\.json['"${'`'}]\\);\\s*(//.*)?`,
    'g'
  );

  const templateRegExp = new RegExp(
    `(^|(?<=[^\\w\\d]))${template}((\\.\\w+)|(\\[['"${'`'}]\\w+['"${'`'}]\\]))`,
    'g'
  );

  const partsRegExp = /((?<=\.)\w+)|(?<=\[['"`]\w+(?=['"`]\]))/g;

  return {
    name: 'rollup-read-package-json',

    transform(code, id) {
      if (!(filter(id) && requireRegExp.test(code))) return;

      code = code
        .replace(requireRegExp, '')
        .replace(templateRegExp, (match) => {
          let value = packageJson;

          [...match.matchAll(partsRegExp)].forEach(
            ([name]) => (value = value[name])
          );

          const tValue = typeof value;

          return 'string' === tValue
            ? `'${value}'`
            : 'number' === tValue
            ? `${value}`
            : `'${JSON.stringify(value)}'`;
        });

      return {
        code,
        map: { mappings: '' },
      };
    },
  };
}

module.exports = rollupReadPackageJson;
