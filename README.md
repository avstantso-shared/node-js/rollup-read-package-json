# rollup-read-package-json

Replaces PACKAGE_JSON and subpaths to values from package.json

## Example

package.json:
```
{
  "name": "the-name-of-this-package",
  ...
}
```
  
Source file fragment (comment is optional):
```
const PACKAGE_JSON = require(`../package.json`); // See @avstantso/node-js--rollup-read-package-json

console.log(PACKAGE_JSON.name);
```
  
After compile file fragment:
```
console.log('the-name-of-this-package');
```